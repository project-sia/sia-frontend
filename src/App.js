import React, { Component } from 'react';
import TopBar from './components/TopBar/TopBar';
import InputControl from './components/InputControl/InputControl';
import GraphDisplay from './components/GraphDisplay/GraphDisplay';
import './App.css';
import { Container } from 'react-bootstrap';
import LoadingOverlay from 'react-loading-overlay';
import { stat } from 'fs';
class App extends Component {

  state = ({
    loaderActive: false
  })

  render() {
    return (
      <div>
        <TopBar/>
        <LoadingOverlay active={this.state.loaderActive}>
          <Container fluid="true">
            <GraphDisplay/>
            <InputControl loader={this.setLoader}/>
          </Container>
        </LoadingOverlay>
      </div>
      
    );
  }

  setLoader = (status) =>{
    this.setState({loaderActive: status})
  }
}

export default App;
