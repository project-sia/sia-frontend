import fetch from 'node-fetch';

export async function getCall(endpoint){
    return await fetch(endpoint)
        .then(response => response.json())
        .catch(error => {
            console.log("Error spotted! ", error);
            return false;
        });
}

export async function postCall(endpoint, body){
    console.log("POST Calling with body:", JSON.stringify(body))
    let overallResponse = await fetch(endpoint, {
        method: 'post',
        body: JSON.stringify(body),
        headers: {'Content-Type': 'application/json'},
    })
        .then(response => response.json())
        .catch(error => {
            console.log("Error spotted! ", error);
            return false;
        })

    return overallResponse;
}

export function patchCall(endpoint, body){
    return fetch(endpoint, {
        method: 'PATCH',
        body: JSON.stringify(body),
        headers: {'Content-Type': 'application/json'},
    })
        .then(response => {
            return response.json()}
            )
        .catch(error => {
            console.log("Error spotted! ", error);
            return false;
        })



}