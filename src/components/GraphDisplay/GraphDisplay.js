import React, { Component } from 'react'
import * as css from './GraphDisplay.css'
import Chart from 'react-apexcharts';
import * as api from '../../Utility/APICallUtility';
import * as endpoint from '../../Utility/APIEndpoint';
export default class GraphDisplay extends Component {

    state = ({
        options: {
            chart: {
            id: "basic-bar"
            },
            xaxis: {
            categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998]
            }
        },
        series: [
            {
                name: "Brand 1 Overall Stock Count",
                data: [30, 40, 45, 50, 49, 60, 70, 91]
            },
            {
                name: "Brand 2 Overall Stock Count",
                data: [30, 40, 45, 50, 49, 60, 70, 42]
            }
        ]
    });

    componentDidMount = async () =>{
        var response = await this.loadGraph()

        
    }
    
    render(){
        return (
            <div className={css.display}>
                <Chart
                    options={this.state.options}
                    series={this.state.series}
                    type="line"
                    width="1230"
                    height="500"
                />
            </div>
        )
    }

    loadGraph = async () => {
        let response = await api.getCall(endpoint.OrderHistoryAPI);
        var categories = [];
        for(const s of response){
            categories = [...categories, s.brandName];
        }
        let carbonCopy = this.state.options;
        carbonCopy.categories = categories;
        this.setState({options: carbonCopy})
        return response;
    }
}