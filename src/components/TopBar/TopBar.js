import React, { Component } from 'react'
import {Navbar} from 'react-bootstrap';

import logo from '../../logo.svg';
export default class TopBar extends Component {
    render(){
        return (
            <Navbar sticky="top" bg="dark" variant="dark">
                <Navbar.Brand href="#home">
                    <img src={logo}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    alt="React Placeholder Logo" />
                    {'Project Sia'}
                </Navbar.Brand>
            </Navbar>
        )
    }
}