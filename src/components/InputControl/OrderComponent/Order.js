import React, { Component } from 'react'
import {Form, Button, Col, Row} from 'react-bootstrap';
import * as css from './Order.css';
import * as api from '../../../Utility/APICallUtility';
import * as endpoints from '../../../Utility/APIEndpoint';
import { cpus } from 'os';
export default class Order extends Component {

    state = ({
        userInput: {
            product: "Pen",
            brand: "Pilot",
            quantity: "1"
        },
        serverData: {

        },

        validation: {
            stockStatus: true
        }
    })

    render() {
        return (
            <div className={css.formBorder}>
                <h3>Order Form</h3>
                <Form noValidate validated={this.state.validation.stockStatus}>
                    <Form.Group controlId="order.product">
                        <Form.Label>Product</Form.Label>
                        <Form.Control required as="select"  onLoad={this.updateProduct} onChange={this.updateProduct}>
                            <option>Pen</option>
                            <option>Pencil</option>
                            <option>Eraser</option>
                            <option>Whiteout</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="order.brand">
                        <Form.Label>Brand</Form.Label>
                        <Form.Control required as="select" onLoad={this.updateBrand} onChange={this.updateBrand}>
                            <option>Pilot</option>
                            <option>Uni</option>
                            <option>Zebra</option>
                            <option>Pentel</option>
                            <option>Staedtler</option>
                            <option>PaperMate</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="order.quantity">
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control required type="text" onBlur={this.checkQuantityValidator} onLoad={this.updateQuantity} placeholder="Enter Quantity in digits" onChange={this.updateQuantity}/>
                    </Form.Group>
                    <Form.Group controlId="order.submission">
                        <Button variant = "outline-secondary" onClick={this.clickSubmission}>Submit</Button>
                    </Form.Group>
                        
                    
                </Form>
            </div>
        )
    }


    clickSubmission = async (event) => {
        this.props.loader(true);
        let response = await api.postCall(endpoints.OrderAPI, 
            {
                brandName: this.state.userInput.brand,
                productName: this.state.userInput.product,
                amount: this.state.userInput.quantity,
            });
        console.log(response);
        this.props.loader(false);
    }

    updateProduct = (val) => {
        console.log(val.target.value);
        const tmpData = this.state.userInput;
        tmpData.product = val.target.value;
        this.setState({userInput: tmpData
        })
    }

    updateBrand = (val) => {
        
        const tmpData = this.state.userInput;
        tmpData.brand = val.target.value;
        this.setState({userInput: tmpData
        })
    }

    updateQuantity = (val) => {
        const tmpData = this.state.userInput;
        tmpData.quantity = val.target.value;
        this.setState({userInput: tmpData
        })
    }

    checkQuantityValidator = async (val) => {
        console.log(val.target.value);
        let response = await api.postCall(endpoints.StockCheckAPI, 
            {
                brandName: this.state.userInput.brand,
                productName: this.state.userInput.product,
                amount: parseInt(val.target.value)
            });
        this.setState({validation: {
            stockStatus: response.stockStatus
        }});
    }

}