import React from 'react'
import { Row, Col } from 'react-bootstrap';
import Restock from './RestockComponent/Restock';
import Order from './OrderComponent/Order';
import * as css from './InputControl.css';
export default (props) => {
    return (
        <Row>
            <Col className={css.alignCenter}>
                <Order loader={props.loader}/>
            </Col>
            <Col>
                <Restock loader={props.loader}/>
            </Col>
  </Row>
    )
  
}
