import React, { Component } from 'react'
import {Form, Button} from 'react-bootstrap';
import * as api from '../../../Utility/APICallUtility';
import * as endpoints from '../../../Utility/APIEndpoint';
export default class Restock extends Component {

    state = ({
        userInput: {
            product: "Pen",
            brand: "Pilot",
            quantity: "0"
        },
        serverData: {

        },
        validated: false
    })

    render() {
        return (
            <div>
                <h3>Restock Form</h3>
                <Form noValidate validated={this.state.validated}>
                    <Form.Group controlId="order.product">
                        <Form.Label>Product</Form.Label>
                        <Form.Control required as="select"  onLoad={this.updateProduct} onChange={this.updateProduct}>
                            <option>Pen</option>
                            <option>Pencil</option>
                            <option>Eraser</option>
                            <option>Whiteout</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="order.brand">
                        <Form.Label>Brand</Form.Label>
                        <Form.Control required as="select" onLoad={this.updateBrand} onChange={this.updateBrand}>
                            <option>Pilot</option>
                            <option>Uni</option>
                            <option>Zebra</option>
                            <option>Pentel</option>
                            <option>Staedtler</option>
                            <option>PaperMate</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="order.quantity">
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control required type="text" onLoad={this.updateQuantity} onBlur={this.checkQuantityValidator} placeholder="Enter Quantity in digits" onChange={this.updateQuantity}/>
                        <Form.Control.Feedback type="invalid">
                            Please enter the amount to restock
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Button variant = "outline-secondary"  onClick={this.clickSubmission}>Submit</Button>
                </Form>
            </div>
          )
    }

    clickSubmission = async (event) => {
        this.props.loader(true);
        let response = await api.patchCall(endpoints.RestockAPI, 
            {
                brandName: this.state.userInput.brand,
                productName: this.state.userInput.product,
                amount: this.state.userInput.quantity,
            });
        console.log(response);
        this.props.loader(false);
    }

    updateProduct = (val) => {
        console.log(val.target.value);
        const tmpData = this.state.userInput;
        tmpData.product = val.target.value;
        this.setState({userInput: tmpData
        })
    }

    updateBrand = (val) => {
        
        const tmpData = this.state.userInput;
        tmpData.brand = val.target.value;
        this.setState({userInput: tmpData
        })
    }

    updateQuantity = (val) => {
        const tmpData = this.state.userInput;
        tmpData.quantity = val.target.value;
        this.setState({userInput: tmpData
        })
    }

    



}
